A Break-Out clone with Pygame.

Objective:
    Our project is to create a breakout game in pygame.

Installation instructions:
    Install with 'pip install brickbreaker', then 
    run 'brickbreaker' to play.

    If you don't have admin privileges, then use 
    'pip install --user brickbreaker', then run 
    'python -m brickbreaker' to play.

    If pip doesn't want to play nice for some reason you can clone the repo:
    git clone https://gitlab.com/uttyler4336group5/cosc4332pythonfinalproject.git
    or download it manually from here:
    https://gitlab.com/uttyler4336group5/cosc4332pythonfinalproject 
    cd into the 'brickbreaker' directory, then run 'python __main__.py' 
    (pygame also needs to be installed, 'pip install --user pygame').

Media credits:
    nebula background and explosion strip - Kim Lathrop
    All of the following media came from www.opengameart.org
        paddle sprite - Aj_
        swamp sunset, desert clouds, wizard tower backgrounds - JAP
        lose ball sound - remaxim
        game over sound - Joseph Pueyo
        all level soundtracks - SketchyLogic
        drain sound (collide) - qubodup
        block explode sound - Luke.RUSTLTD
    beach ball graphic - pygame.org 
        https://www.pygame.org/docs/tut/PygameIntro.html

List of files:
    brickbreaker/__init__.py - Marks the directory as a package directory;
        allows python to import modules from here.
    brickbreaker/__main__.py - Script to run the game. Execute this to play.
    brickbreaker/assets/jingles/game_complete.ogg - Short in-game audio clip.
    brickbreaker/assets/jingles/game_over.ogg - Short in-game audio clip.
    brickbreaker/assets/jingles/level_complete.ogg - Short in-game audio clip.
    brickbreaker/assets/level_0/background.png - Level background image.
    brickbreaker/assets/level_0/soundtrack.ogg - Level music track.
    brickbreaker/assets/level_0/soundtrack_intro.ogg - Level music track.
    brickbreaker/assets/level_1/background.png - Level background image.
    brickbreaker/assets/level_1/soundtrack.ogg - Level music track.
    brickbreaker/assets/level_2/background.png - Level background image.
    brickbreaker/assets/level_2/soundtrack.ogg - Level music track.
    brickbreaker/assets/level_3/background.png - Level background image.
    brickbreaker/assets/level_3/soundtrack.ogg - Level music track.
    brickbreaker/assets/level_3/soundtrack_intro.ogg - Level music track.
    brickbreaker/assets/se/block_explode.ogg - Short in-game audio clip.
    brickbreaker/assets/se/lose_ball.ogg - Short in-game audio clip.
    brickbreaker/assets/se/lose_final_ball.ogg - Short in-game audio clip.
    brickbreaker/assets/se/paddle_wall_collide.ogg - Short in-game audio clip.
    brickbreaker/assets/sprites/ball.gif - Image file of ball.
    brickbreaker/assets/sprites/bluepaddle.png - Image file of paddle.
    brickbreaker/assets/sprites/brownpaddle.png - Alternate image file of paddle.
    brickbreaker/ball.py - Holds the class for the breakout ball.
    brickbreaker/block.py - Holds the classes for the breakout blocks.
    brickbreaker/game_manager.py - Manages game execution.
    brickbreaker/hud.py - Docked display to show current player and level info.
    brickbreaker/level.py - Holds the level class to store level info.
    brickbreaker/message.py - Shows a message to the player between levels.
    brickbreaker/object_definitions.py - Holds info about game assets.
    brickbreaker/paddle.py - Holds the class for the breakout paddle.
    brickbreaker/scores.sqlite - Stores the highscore in sqlite db.
    brickbreaker/sound.py - Classes to store and play music and sound effects.
    brickbreaker/timer.py - A class for counting time.
    brickbreaker/util.py - Useful functions for game objects.
    LICENSE.txt - Game license.
    MANIFEST.in - Lists data files for packaging for PyPi.
    README.md - A ton of info about the files in the game.
    setup.py - Script for creating PyPi package.
